#-------------------------------------------------
#
# Project created by QtCreator 2014-07-07T10:59:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4) {
    QT       += widgets serialport
} else {
    include($$QTSERIALPORT_PROJECT_ROOT/src/serialport/qt4support/serialport.prf)
}

TARGET = TempGraph
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tempModel.cpp \
    settingsdialog.cpp

HEADERS  += mainwindow.h \
    tempModel.h \
    settingsdialog.h

FORMS    += mainwindow.ui \
    settingsdialog.ui
