#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QtGlobal>

#include <QMainWindow>

#include <QtSerialPort/QSerialPort>

#include "TempModel.h"

QT_BEGIN_NAMESPACE

namespace Ui {
class MainWindow;
}

QT_END_NAMESPACE

class SettingsDialog;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void about();
    void handleError(QSerialPort::SerialPortError error);
    void closeSerialPort();
    void readData();
    void openSerialPort();
private:
    void initActionsConnections();
    void SetUpTable();


private:
    Ui::MainWindow *ui;
    TempModel *tempModel;
    SettingsDialog *settings;
    QSerialPort *serial;
};

#endif // MAINWINDOW_H
