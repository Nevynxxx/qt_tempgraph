#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdialog.h"

#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    serial = new QSerialPort(this);

    settings = new SettingsDialog;

    tempModel = new TempModel(0);

    ui->action_Connect->setEnabled(true);
    ui->action_Disconnect->setEnabled(false);
    ui->actionQuit->setEnabled(true);
    ui->actionConfigure->setEnabled(true);

    ui->tempDataView->setModel( tempModel );
    SetUpTable();

    ui->tempDataView->show();
    ui->tempDataView->adjustSize();
    QString msg = QString("Current Column Width: %1").arg(ui->tempDataView->columnWidth(0));

    ui->statusBar->showMessage(msg);

    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));
    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));

    initActionsConnections();
}

MainWindow::~MainWindow()
{
  delete settings;
  delete ui;
  delete tempModel;
  delete serial;
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Temp Graph"),
                       tr("A simple application to take Temperature from the serial "
                          "port and graph it. "));
}

void MainWindow::initActionsConnections()
{
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionConfigure, SIGNAL(triggered()), settings, SLOT(show()));
    connect(ui->action_Connect, SIGNAL(triggered()), this, SLOT(openSerialPort()));
    connect(ui->action_Disconnect, SIGNAL(triggered()), this, SLOT(closeSerialPort()));
}

// Initial Data setup
void MainWindow::SetUpTable()
{
    for (int i=0;i<MAXDATA;i++){
        QTime time = QTime::currentTime();
        dataPoint d;
        d.temp = 0;
        d.time = time;
        tempModel->addDataPoint(d);
    }
}

void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}

void MainWindow::closeSerialPort()
{
    serial->close();
    ui->action_Connect->setEnabled(true);
    ui->action_Disconnect->setEnabled(false);
    ui->actionConfigure->setEnabled(true);
    ui->statusBar->showMessage(tr("Disconnected"));
}

void MainWindow::readData()
{
    while(serial->canReadLine()){
      QByteArray data = serial->readAll();
      console->putData(data);
    //  ui->textBrowser->append(data);
      QTime time = QTime::currentTime();
      dataPoint d;
      d.temp = data[0];
      d.time = time;
      tempModel->addDataPoint(d,0);
      QString text = time.toString("hh:mm:ss.zzz") + ":" +data;
      ui->textBrowser->append(text);
      }
}
void MainWindow::openSerialPort()
{
    SettingsDialog::Settings p = settings->settings();
    serial->setPortName(p.name);
    if (serial->open(QIODevice::ReadWrite)) {
        if (serial->setBaudRate(p.baudRate)
                && serial->setDataBits(p.dataBits)
                && serial->setParity(p.parity)
                && serial->setStopBits(p.stopBits)
                && serial->setFlowControl(p.flowControl)) {

            ui->action_Connect->setEnabled(false);
            ui->action_Disconnect->setEnabled(true);
            ui->actionConfigure->setEnabled(false);
            ui->statusBar->showMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                                       .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                                       .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));

        } else {
            serial->close();
            QMessageBox::critical(this, tr("Error"), serial->errorString());

            ui->statusBar->showMessage(tr("Open error"));
        }
    } else {
        QMessageBox::critical(this, tr("Error"), serial->errorString());

        ui->statusBar->showMessage(tr("Configure error"));
    }
}
