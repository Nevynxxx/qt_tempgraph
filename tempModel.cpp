// mymodel.cpp
#include "tempModel.h"



TempModel::TempModel(QObject *parent)
    :QAbstractTableModel(parent)
{
}

int TempModel::rowCount(const QModelIndex & /*parent*/) const
{
   return MAXDATA;
}

int TempModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 2;
}
// Return the value of a given cell. Cell location given by vector "Index"
QVariant TempModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
         {
            int row = (MAXDATA - 1) - index.row();
            if (index.column() == 0)
                {return datapoints[row].time;}
            else
                {return datapoints[row].temp;}
         }
    return QVariant();
}

//Set Column Headers
QVariant TempModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return QString("Time");
            case 1:
                return QString("Temp (C)");

            }
        }
    }
    return QVariant();
}
// given aa row (index) and a structure with data. Set the data point.
void TempModel::addDataPoint(dataPoint data)
{
    datapoints.enqueue(data);
    if(datapoints.count() > MAXDATA) {datapoints.dequeue();}
}
