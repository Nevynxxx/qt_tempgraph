#ifndef TEMPMODEL_H
#define TEMPMODEL_H
// tempModel.h
// Model to hold the data that comes in
#include <QAbstractTableModel>
#include <QDateTime>
#include <QQueue>

#define MAXDATA 50

// The data we want to hold.
struct dataPoint {
    QTime time;
    int temp;
};

class TempModel : public QAbstractTableModel
 {
     Q_OBJECT
    // This is a table of the objects we created above to hold the data.
    QQueue <dataPoint> datapoints;
 public:
     TempModel(QObject *parent);
     int rowCount(const QModelIndex &parent = QModelIndex()) const ;
     int columnCount(const QModelIndex &parent = QModelIndex()) const;
     QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
     QVariant headerData(int section, Qt::Orientation orientation, int role) const;
     void addDataPoint(dataPoint data);

 private:

 };
#endif // TEMPMODEL_H
